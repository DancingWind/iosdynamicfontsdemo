//
//  StyleDemoCell.swift
//  Text Style Demo
//
//  Created by Martynas Stanaitis on 2020-05-07.
//  Copyright © 2020 Martynas Stanaitis. All rights reserved.
//

import UIKit

class StyleDemoCell: UITableViewCell {

    @IBOutlet weak var styleName: UILabel!
    @IBOutlet weak var styleExample: UILabel!
    @IBOutlet weak var fontDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func populate(name: String, font: UIFont, exampleText: String = "Example text 123!?") {
        styleName.text = name
        styleExample.text = exampleText
        styleExample.font = font
        fontDescription.text = "\(font)"
    }
    
}
