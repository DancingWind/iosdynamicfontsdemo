//
//  ViewController.swift
//  Text Style Demo
//
//  Created by Martynas Stanaitis on 2020-05-07.
//  Copyright © 2020 Martynas Stanaitis. All rights reserved.
//

import UIKit


struct FontStyle {
    let name: String
    let font: UIFont
}


class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let list = [
        FontStyle(name: "Large Title", font: UIFont.preferredFont(forTextStyle: .largeTitle)),
        FontStyle(name: "Title 1", font: UIFont.preferredFont(forTextStyle: .title1)),
        FontStyle(name: "Title 2", font: UIFont.preferredFont(forTextStyle: .title2)),
        FontStyle(name: "Title 3", font: UIFont.preferredFont(forTextStyle: .title3)),
        FontStyle(name: "Headline", font: UIFont.preferredFont(forTextStyle: .headline)),
        FontStyle(name: "SubHeadline", font: UIFont.preferredFont(forTextStyle: .subheadline)),
        FontStyle(name: "Body", font: UIFont.preferredFont(forTextStyle: .body)),
        FontStyle(name: "Callout", font: UIFont.preferredFont(forTextStyle: .callout)),
        FontStyle(name: "Footnote", font: UIFont.preferredFont(forTextStyle: .footnote)),
        FontStyle(name: "Caption 1", font: UIFont.preferredFont(forTextStyle: .caption1)),
        FontStyle(name: "Caption 2", font: UIFont.preferredFont(forTextStyle: .caption2)),
        
        //MARK: edit here
        FontStyle(name: "Custom Body - light 15.0",
                  font: UIFontMetrics
                    .init(forTextStyle: .body)
                    .scaledFont(for: UIFont.systemFont(ofSize: 15.0,
                                                       weight: .light)))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50.0
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "StyleDemoCell", bundle: .main),
                           forCellReuseIdentifier: "StyleDemoCell")
        tableView.tableFooterView = UIView()
    }

    

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = list[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "StyleDemoCell") as! StyleDemoCell
        cell.populate(name: item.name, font: item.font)
        return cell
    }
    
    
}

